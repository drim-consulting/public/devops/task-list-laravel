// В качестве базового образа используйте php:7.2.7-fpm

RUN apt-get update \
  && mkdir -p /usr/share/man/man1 \
  && mkdir -p /usr/share/man/man7 \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  postgresql-client \
  libpq-dev \
  libfreetype6-dev \
  libjpeg62-turbo-dev \
  libmcrypt-dev \
  libbz2-dev \
  cron \
  && pecl channel-update pecl.php.net \
  && pecl install apcu

RUN docker-php-ext-install zip pdo_pgsql \
  && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
  && docker-php-ext-install gd

RUN echo "display_errors=stderr" > $PHP_INI_DIR/conf.d/display-errors.ini

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

// Скопируйте файл composer.json в /var/www/html

RUN composer install --no-scripts --no-dev --prefer-dist -vvv \
  && rm -rf /root/.composer/cache

// Скопируйте все файлы из текущей директории в /var/www/html

// Установите рабочей директорией /var/www/html

RUN touch storage/logs/laravel.log

RUN chmod -R 777 /var/www/html/storage

RUN php artisan

// Установите начальной командой запуска контейнера ["/bin/sh", "-c", "php-fpm -D | tail -f storage/logs/laravel.log"]
