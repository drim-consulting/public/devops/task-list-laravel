WEB_IMAGE_TAG=$CI_REGISTRY_IMAGE/web:$IMAGE_VERSION
APP_IMAGE_TAG=$CI_REGISTRY_IMAGE/app:$IMAGE_VERSION

docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY

docker build -t $WEB_IMAGE_TAG -f ./src/app/web.Dockerfile ./src/app
docker build -t $APP_IMAGE_TAG -f ./src/app/app.Dockerfile ./src/app

docker push $WEB_IMAGE_TAG
docker push $APP_IMAGE_TAG
