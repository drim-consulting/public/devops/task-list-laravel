# GitLab CI

## Цель

Создать GitLab CI pipeline для автоматизации развертывания приложений согласно Git Flow

## Шаги

### Зарегистрируйте GitLab Runner

- Скопируйте токен со страницы `Settings -> CI/CD -> Runners`. На этой же странице нажмите `Disable shared Runners`

- Зайдите на виртуальную машину

- Выполните команду `sudo gitlab-runner register` и зарегистрируйте runner, указав `docker`, `k8s`, `helm` в качестве тегов, скопированный токен в качестве токена, `shell` в качестве executor

- Убедитесь, что runner появился на странице `Settings -> CI/CD -> Runners`

### Доработайте Helm чарт

- Добавьте файл `ingress.yml` из папки с инструкцией в папку `templates`

- Скопируйте все манифесты из предыдущего задания в папку `templates`

- Добавьте в `app.yml`, `web.yml` и `migration.yml` новую метку `commit: {{ .Values.commit }}` по пути `spec.template.metadata.labels`

- Измените название `Job` в файле `migration.yml` на `name: migration-{{ .Values.commit }}`

- Измените поле у `Service` в файле `web.yml` с `nodePort: XYZ` на `targetPort: 80`

- Укажите в `app.yml`, `web.yml` и `migration.yml` версию образа в виде `{{ .Values.version }}`

### Скопируйте скрипты и определения CI/CD

- Скопируйте папку `scripts` из папки с инструкциями в папку `devops` репозитория

- Скопируйте файл `.gitlab-ci.yml` из папки с инструкциями в корневую папку репозитория

- Создайте новый Deploy Token на странице `Settings -> Repository -> Deploy Tokens`. Укажите `gitlab-deploy-token` в качестве имени и выберите `read_registry` в `Scopes`

- Скопируйте значение из поля `Username` только что созданного токена и замените им значение в строке `--docker-username=gitlab+deploy-token-8760 \` файла `devops\scripts\deploy.sh`

### Запустите pipeline для feature branch

- Закоммитьте все изменения

- Создайте новую ветку `feature/deploy` со всеми изменениями

- Отправьте ветку в GitLab с помощью команды `git push`

- Убедитесь, что был создан и успешно выполнен экземпляр конвейера на странице `CI\CD -> Pipelines`

- Убедитесь, что новое окружение создано и доступно на странице `Operations -> Environments`

### Запустите pipeline для остальных типов веток

- Доработайте файл `.gitlab-ci.yml` для того, чтобы конвейер запускался для веток `develop`, `release/` и `master`
