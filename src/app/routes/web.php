<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function() {
    return redirect('tasks');
});

Route::get('/tasks', function () {
    $tasks = DB::table('tasks')->get();
    return view('tasks.index', compact('tasks'));
});

Route::get('/tasks/{task}', function ($id) {
    $task = DB::table('tasks')->find($id);
    return view('tasks.show', compact('task'));
});

Route::post('/tasks', function (Request $request) {
    $body = $request->input('body');

    DB::table('tasks')->insert([
        'body' => $body,
        'created_at' => 'NOW()',
        'updated_at' => 'NOW()',
    ]);

    return redirect('tasks');
});
