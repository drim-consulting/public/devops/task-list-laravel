<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task List</title>
</head>
<body>
    <h1>Мои задачи</h1>
    <div>
        <form action="/tasks" method="post">
            {{ csrf_field() }}
            <input type="text" name="body"/>
            <input type="submit" value="Добавить"/>
        </form>
    </div>
    <ul>
        @foreach ($tasks as $task)
            <li><a href="tasks/{{ $task->id }}">{{ $task->body }}</a></li>
        @endforeach
    </ul>
</body>
</html>
